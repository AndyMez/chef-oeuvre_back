module.exports = {
  root: true,
  extends: ['standard', 'prettier'],
  globals: {
    IS_DEVELOPMENT: 'readonly'
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: "module"
  }
}
