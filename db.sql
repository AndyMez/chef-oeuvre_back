USE models;

-- Table user
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`(
    `id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `email` varchar(64) NOT NULL,
    `password` varchar(64) NOT NULL,
    `role` varchar(64) NOT NULL
);

INSERT INTO `user` (`email`, `password`, `role`)
VALUES
('test@test.fr', 'test', 'admin'),
('user@user.fr', 'user', 'user');


--Table models
DROP TABLE IF EXISTS `model`;

CREATE TABLE `model`(
    `id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(255),
    `sexe` VARCHAR(64),
    `height` INT,
    `weight` INT,
    `bust` INT,
    `waist` INT,
    `hips` INT,
    `eyes` VARCHAR(64),
    `hair` VARCHAR(64),
    `photo_id` INT,
    `photographer_id` INT,
    `experience_id` INT
);

INSERT INTO `model` (`name`, `sexe`, `height`, `weight`, `bust`, `waist`, `hips`, `eyes`, `hair`, `photo_id`, `photographer_id`, `experience_id`)
VALUES
('Louise Bordeaux', 'Femme', '165', '55', 80, 50, 85, 'Marron', 'Brune', 1, 1, 1 ),
('Christien Talon', 'Homme', '175', '77', NULL, NULL, NULL, 'Blond', 'Bleu', 2, 2, 2 );


--Table photographers
DROP TABLE IF EXISTS `photographer`;

CREATE TABLE `photographer`(
    `id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(255),
    `model_id` INT,
    `photo_id` INT,
    `experience_id` INT
);

INSERT INTO `photographer` (`name`, `model_id`, `photo_id`, `experience_id`)
VALUES
('Photographe 1', 1, 1, 1),
('Photographe 2', 2, 2, 2);

--Table photos
DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo`(
    `id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `photo` VARCHAR(255)
);

INSERT INTO `photo` (`photo`)
VALUES
('https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8bW9kZWx8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60'),
('https://images.unsplash.com/photo-1604514628550-37477afdf4e3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bW9kZWxzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60'),
('https://images.unsplash.com/photo-1493863641943-9b68992a8d07?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG9ncmFwaGVyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60'),
('https://images.unsplash.com/photo-1551737823-dfc8495904b4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzR8fHBob3RvZ3JhcGhlcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=600&q=60');



--Table experiences
DROP TABLE IF EXISTS `experience`;

CREATE TABLE `experience`(
    `id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `experience` TEXT
);

INSERT INTO `experience` (`experience`)
VALUES
('Profile 1 : My experience is ..., and she was ...'),
('Profile 2 : My experience is ..., and she was ...'),
('Profile 3 : My experience is ..., and she was ...'),
('Profile 4 : My experience is ..., and she was ...');

