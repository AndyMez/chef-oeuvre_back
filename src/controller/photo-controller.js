import { Router } from "express";
import { PhotoRepository } from "../repository/photo-repository";


export const photoController = Router();


photoController.get('/', async (req,res) => {
  try {
    let result = await new PhotoRepository().findAll();
    res.json(result)
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})


photoController.get('/:id', async (req, res) => {
  try {
    let result = await new PhotoRepository().findById(req.params.id)
    res.json(result)
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})
