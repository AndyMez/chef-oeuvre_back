import { Router } from "express";
import { PhotographerRepository } from "../repository/photographer-repository";


export const photographerController = Router();


photographerController.get('/', async (req, res) => {
  try {
    let result = await new PhotographerRepository().findAll();
    res.json(result)
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})


photographerController.get('/:id', async (req, res) => {
  try {
    let result = await new PhotographerRepository().findById(req.params.id);
    res.json(result)
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})
