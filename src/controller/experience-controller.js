import { json, Router } from 'express'
import { ExperienceRepository } from '../repository/experience-repository'

export const experienceController = Router()

experienceController.get('/', async (req, res) => {
  try {
    let result = await new ExperienceRepository().findAll()
    res.json(result)
    console.log(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

experienceController.get('/:id', async (req, res) => {
  try {
    let result = await new ExperienceRepository().findById(req.params.id)
    res.json(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

// experienceController.post('/', async (req, res) => {
//   try {
//     let id = await new ExperienceRepository().add(req.body)
//     res.status(201).json(req.body)
//   } catch (error) {
//     console.log(error);
//     res.status(500).json(error)
//   }
// })

// experienceController.patch('/', async (req, res) => {
//   try {
//     await new ExperienceRepository().update(req.body)
//     res.end()
//     res.status(204)
//   } catch (error) {
//     console.log(error);
//     res.status(500).json(error)
//   }
// })

// experienceController.delete('/:id', async (req, res) => {
//   try {
//     await new ExperienceRepository().delete(req.params.id)
//     res.status(204)
//   } catch (error) {
//     console.log(error);
//     res.status(500).json(error)
//   }
// })
