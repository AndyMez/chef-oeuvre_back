import { Router } from 'express'
import { CastingRepository } from '../repository/casting-repository'

export const castingController = Router()

castingController.get('/', async (req, res) => {
  try {
    let result = await new CastingRepository().findAll()
    res.json(result)
    console.log(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

castingController.get('/:id', async (req, res) => {
  try {
    let result = await new CastingRepository().findById(req.params.id)
    res.json(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

castingController.post('/', async (req, res) => {
  try {
    let result = await new CastingRepository().add(req.body)
    res.status(201).json(req.body)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})
