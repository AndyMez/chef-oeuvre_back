import { Router } from 'express'
import { ModelRepository } from '../repository/model-repository'

export const modelController = Router()

modelController.get('/', async (req, res) => {
  try {
    let result = await new ModelRepository().findAll()
    res.json(result)
    console.log(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

modelController.get('/:id', async (req, res) => {
  try {
    let result = await new ModelRepository().findById(req.params.id)
    res.json(result)
    console.log(result)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

modelController.post('/', async (req, res) => {
  try {
    let id = await new ModelRepository().add(req.body)
    res.status(201).json(req.body)
  } catch (error) {
    console.log(error)
    res.status(500).json(error)
  }
})

