import { Router } from 'express'
import { User, userSchema } from '../entity/User'
import { UserRepository } from '../repository/user-repository'
import bcrypt from 'bcrypt'
import { generateToken, protect } from '../utils/token'
import passport from 'passport'

export const userController = Router()


userController.get('/', protect(), async (req, res) => {
  try {
    const users = await UserRepository.findAll();
    res.json(users)
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})

/**
 * Route sécurisée à l'aide de Passport. L'accès sera refusé sans JWT ou Token invalide
 */
userController.get('/account', passport.authenticate('jwt', {session:false}), (req, res) => {
  res.json(req.user)
})

userController.post('/', async (req, res) => {
  try {
    const {error} = userSchema.validate(req.body, {abortEarly:false});

    if (error) {
      res.status(400).json({error: error.details.map(item => item.message)})
      return;
    }

    const newUser = new User();
    Object.assign(newUser, req.body)

    const exists = await UserRepository.findByEmail(newUser.email)
    if (exists) {
      res.status(400).json({error: 'Email already taken'})
      return
    }

    newUser.role = 'user';

    newUser.password = await bcrypt.hash(newUser.password, 11);
    await UserRepository.add(newUser)
    res.status(201).json({
      user: newUser,
      token: generateToken({
        email: newUser.email,
        id: newUser.id,
        role: newUser.role
      })
    })

  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})

userController.post('/login', async (req, res) => {
  try {
    const user = await UserRepository.findByEmail(req.body.email)
    if (user) {
      const samePassword = await bcrypt.compare(req.body.password, user.password);
      if(samePassword) {
        res.json({
          user,
          token: generateToken({
            email: user.email,
            id: user.id,
            role: user.role
          })
        })
        return;
      }
    }
    res.status(401).json({error: 'Wrong email and/or password'});
  } catch (error) {
    console.log(error);
    res.status(500).json(error)
  }
})
