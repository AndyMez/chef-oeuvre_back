import Joi from "joi"

export class User {
  id
  email
  password
  role

  constructor(email, password, role = 'user', id = null) {
    this.email = email;
    this.password = password;
    this.role = role;
    this.id = id;
  }


  toJSON() {
    return {
      id: this.id,
      email: this.email,
      role: this.role
    }
  }
}

export const userSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(4).required()
});
