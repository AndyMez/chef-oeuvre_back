export class Experience {
  experience_id;
  model_id;
  experience;

  constructor(experience_id, model_id, experience) {
    this.experience_id = experience_id;
    this.model_id = model_id;
    this.experience = experience;
  }
}
