export class Photo {
  photo_id;
  model_id;
  photo;

  constructor(photo_id, model_id, photo) {
    this.photo_id = photo_id;
    this.model_id = model_id;
    this.photo = photo;
  }
}
