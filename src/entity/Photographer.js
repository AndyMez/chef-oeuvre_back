export class Photographer {
  id;
  name;
  model_id;
  photo_id;
  experience_id;

  constructor(name, model_id, photo_id, experience_id, id) {
    this.name = name;
    this.model_id = model_id;
    this.photo_id = photo_id;
    this.experience_id = experience_id;
    this.id = id;
  }
}
