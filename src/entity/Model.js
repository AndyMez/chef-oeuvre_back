export class Model {
  model_id;
  name;
  sexe;
  height;
  weight;
  bust;
  waist;
  hips;
  eyes;
  hair;
  photo_id;
  experience_id;

  constructor (model_id, name, sexe, height, weight, bust, waist, hips, eyes, hair, photo_id, experience_id) {
    this.model_id = model_id;
    this.name = name;
    this.sexe = sexe;
    this.height = height;
    this.weight = weight;
    this.bust = bust;
    this.waist = waist;
    this.hips = hips;
    this.eyes = eyes;
    this.hair = hair;
    this.photo_id = photo_id;
    this.experience_id = experience_id;
  }
}
