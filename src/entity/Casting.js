export class Casting {
  id;
  name;
  sexe;
  height;
  weight;
  bust;
  waist;
  hips;
  eyes;
  hair;
  photo;
  experience;

  constructor (id, name, sexe, height, weight, bust, waist, hips, eyes, hair, photo, experience) {
    this.id = id;
    this.name = name;
    this.sexe = sexe;
    this.height = height;
    this.weight = weight;
    this.bust = bust;
    this.waist = waist;
    this.hips = hips;
    this.eyes = eyes;
    this.hair = hair;
    this.photo = photo;
    this.experience = experience;
  }
}
