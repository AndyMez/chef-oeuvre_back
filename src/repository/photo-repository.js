import { Photo } from "../entity/Photo";
import { connection } from "./connection";


export class PhotoRepository {

  async findAll() {
    const [rows] = await connection.query('SELECT * FROM photo')

    return rows.map(row => new Photo(row['photo'], row['id']));
  }


  async findById(id) {
    const [rows] = await connection.query('SELECT * FROM photo WHERE id=?', [id]);

    return rows.map(row => new Photo(row['photo'], row['id']));
  }
}
