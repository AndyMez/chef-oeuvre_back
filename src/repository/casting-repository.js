import { Casting } from "../entity/Casting";
import { connection } from "./connection";

export class CastingRepository {

  async findAll() {
    const [rows] = await connection.query('SELECT * FROM casting')
    const models = []
    for (const row of rows) {
      let model = new Casting()
      model.id = row['id']
      model.name = row['name']
      model.sexe = row['sexe']
      model.height = row['height']
      model.weight = row['weight']
      model.bust = row['bust']
      model.waist = row['waist']
      model.hips = row['hips']
      model.eyes = row['eyes']
      model.hair = row['hair']
      model.photo = row['photo']
      model.experience = row['experience']
      models.push(model)
    }
    return models
  }

  async findById(id) {
    const [rows] = await connection.query('SELECT * FROM casting WHERE id=?', [id])
    const models = []
    for (const row of rows) {
      let model = new Casting()
      model.id = row['id']
      model.name = row['name']
      model.sexe = row['sexe']
      model.height = row['height']
      model.weight = row['weight']
      model.bust = row['bust']
      model.waist = row['waist']
      model.hips = row['hips']
      model.eyes = row['eyes']
      model.hair = row['hair']
      model.photo = row['photo']
      model.experience = row['experience']
      models.push(model)
    }
    return models
  }

  async add(model) {
    const [rows] = await connection.query('INSERT INTO casting (name, sexe, height, weight, bust, waist, hips, eyes, hair, photo, experience) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [model.name, model.sexe, model.height, model.weight, model.bust, model.waist, model.hips, model.eyes, model.hair, model.photo, model.experience])
    return model.id = rows.insertId
  }
}
