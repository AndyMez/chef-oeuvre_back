import { Experience } from "../entity/Experience";
import { Model } from "../entity/Model";
import { Photo } from "../entity/Photo";
import { connection } from "./connection";
import { ExperienceRepository } from "./experience-repository";


export class ModelRepository {

  async findAll() {
    const [rows] = await connection.query('SELECT model.* , photo.photo, photo.model_id, photo.photo_id, experience.experience, experience.model_id, experience.experience_id FROM model LEFT JOIN photo ON photo.photo_id = model.model_id LEFT JOIN experience ON experience.model_id = model.experience_id')
    const models = []
    for (const row of rows) {
      let model = models.find(item => item.model_id == row.model_id)
      if (!model) {
        model = new Model(row.model_id, row.name, row.sexe, row.height, row.weight, row.bust, row.waist, row.hips, row.eyes, row.hair, row.photo_id, row.experience_id)
        model.photo = new Photo(row.photo_id, row.model_id, row.photo)
        model.experience = new Experience(row.experience_id, row.model_id, row.experience)
        models.push(model)
      }
    }
    return models
  }

  // WORK
  // SELECT model.*, photo.photo, photo.photo_id, experience.experience, experience.experience_id FROM model LEFT JOIN photo ON photo.photo_id = model.photo_id LEFT JOIN experience ON experience.experience_id = model.experience_id
  // SELECT model.*, photo.*, experience.*, FROM model LEFT JOIN photo ON photo.model_id = model.photo_id LEFT JOIN experience ON experience.model_id = model.experience_id
  // TEST
  // SELECT model.*, photo.photo, photo.model_id, photo.photo_id, experience.experience, experience.model_id, experience.experience_id FROM model LEFT JOIN photo ON photo.model_id = model.photo_id LEFT JOIN experience ON experience.model_id = model.experience_id
  // SELECT model.*, photo.photo, photo.model_id, photo.photo_id, experience.experience, experience.model_id, experience.experience_id FROM model LEFT JOIN photo ON photo.model_id = model.photo_id LEFT JOIN experience ON experience.model_id = model.experience_id

  // async findAllModel() {
  //   const [rows] = await connection.query('SELECT model.*, photo.photo, photo.photo_id FROM model LEFT JOIN photo ON photo.photo_id = model.photo_id')
  //   const models = []
  //   for (const row of rows) {
  //     let model = models.find(item => item.model_id == row.model_id)
  //     if (!model) {
  //       model = new Model(row.model_id, row.name, row.sexe, row.height, row.weight, row.bust, row.waist, row.hips, row.eyes, row.hair, row.photo_id)
  //       model.photo = new Photo(row.photo, row.photo_id)
  //       models.push(model)
  //     }
  //     // ajouter quelques lignes pour évité que le contenu se répète : voir Git relation Jean
  //   }
  //   return models
  // }
  // SELECT model.*, photo.photo, photo.photo_id, experience.experience, experience.experience_id FROM model LEFT JOIN photo ON photo.photo_id = model.photo_id LEFT JOIN experience ON experience.experience_id = model.experience_id

  //

  // SELECT model.* , photo.photo, photo.model_id, photo.photo_id, experience.experience, experience.model_id, experience.experience_id FROM model LEFT JOIN photo ON photo.photo_id = model.photo_id LEFT JOIN experience ON experience.experience_id = model.experience_id WHERE model.model_id =?
  async findById(id) {
    const [rows] = await connection.query('SELECT model.* , photo.photo, photo.model_id, photo.photo_id, experience.experience, experience.model_id, experience.experience_id FROM model LEFT JOIN photo ON photo.photo_id = model.model_id LEFT JOIN experience ON experience.model_id = model.experience_id WHERE model.model_id =?', [id])
    const models = []
    for (const row of rows) {
      let model = models.find(item => item.model_id == row.model_id)
      if (!model) {
        model = new Model(row.model_id, row.name, row.sexe, row.height, row.weight, row.bust, row.waist, row.hips, row.eyes, row.hair, row.photo_id, row.experience_id)
        model.photo = new Photo(row.photo_id, row.model_id, row.photo)
        model.experience = new Experience(row.experience_id, row.model_id, row.experience)
        models.push(model)
      }
    }
    return models
  }


  // async findById(id) {
  //   const [rows] = await connection.query('SELECT * FROM model WHERE id=?', [id])

  //   return rows.map(row =>new Model(row['name'], row['sexe'], row['height'], row['weight'], row['bust'], row['waist'], row['hips'], row['eyes'], row['hair'], row['photo_id'], row['photographer_id'], row['experience_id'], row['id']))
  // }


  // async add(model) {
  //   const [rows] = await connection.query('INSERT INTO model (name, sexe, height, weight, bust, waist, hips, eyes, hair, photo_id, photographer_id, experience_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [model.name, model.sexe, model.height, model.weight, model.bust, model.waist, model.hips, model.eyes, model.hair, model.photo_id, model.photographer_id, model.experience_id]);
  //   return model.id = rows.insertId
  // }

}
