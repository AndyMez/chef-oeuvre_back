import { Photographer } from "../entity/Photographer";
import { connection } from "./connection";


export class PhotographerRepository {

  async findAll() {
    const [rows] = await connection.query('SELECT * FROM photographer')

    return rows.map(row => new Photographer(row['name'], row['photo'], row['experience'], row['id']))
  }


  async findById(id) {
    const [rows] = await connection.query('SELECT * FROM photographer WHERE id=?', [id])

    return rows.map(row => new Photographer(row['name'], row['photo'], row['experience'], row['id']))
  }
}
