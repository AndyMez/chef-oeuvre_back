import { Experience } from "../entity/Experience";
import { connection } from './connection';
import { ModelRepository } from "./model-repository";

export class ExperienceRepository {

  async findAll() {
    const [rows] = await connection.query('SELECT * FROM experience')
    const experiences = []
    for (const row of rows) {
      let experience = new Experience(row.experience_id, row.experience)
      experiences.push(experience)
    }
    return experiences;
  }


  async findById(id) {
    const [rows] = await connection.query('SELECT * FROM experience WHERE id=?', [id])
    const experiences = [];
    for (const row of rows) {
      let experience = new Experience();
      experience.id = row['id'];
      experience.experience = row['experience'];
      experiences.push(experience)
    }
    return experiences
  }


  async add(experience) {
    const [rows] = await connection.query('INSERT INTO experiences (experience) VALUES (?)', [experience.experience]);
    return experience.id = rows.insertId
  }


    // async findAll() {
  //   const [rows] = await connection.query('SELECT * FROM experience')
  //   const experiences = [];
  //   for (const row of rows) {
  //     let experience = new Experience();
  //     experience.id = row['id'];
  //     experience.experience = row['experience'];
  //     experiences.push(experience)
  //   }
  //   return experiences;
  // }


  // async update(update) {
  //   const [rows] = await connection.query('UPDATE experiences SET experience=? WHERE id=?', [update.experience, update.id])
  // }


//   async delete(id) {
//     const [rows] = await connection.query('DELETE FROM experiences WHERE id=?', [id])
//   }
}
