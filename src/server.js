import express from 'express';
import cors from 'cors';
import { castingController } from './controller/casting-controller';
import { experienceController } from './controller/experience-controller';
import { modelController } from './controller/model-controller';
import { photoController } from './controller/photo-controller';
import { photographerController } from './controller/photographer-controller';
import { userController } from './controller/user-controller';
import { configurePassport } from './utils/token';
import passport from 'passport';

export const server = express();

configurePassport()
server.use(passport.initialize());

server.use(express.json());
server.use(cors());

server.use('/api/casting', castingController)
server.use('/api/experience', experienceController);
server.use('/api/model', modelController);
server.use('/api/photo', photoController);
server.use('/api/photographer', photographerController);
server.use('/api/user', userController);
